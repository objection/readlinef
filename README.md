# Readlinef

## What

It's just some wrapper functions around readline. There's:

* readlinef, which takes a printf-style string
* readlinef\_confirm which does the same and loops until it gets a 'y'
  or 'n' from the user.
* readlinef\_ap, which takes a va\_list

You need to use a gnu-compatible compiler. Or remove "#define
\_GNU\_SOURCE" and change the vasprintf into vsprintfs. I've drunk the
gcc Kool-aid.

The code is pretty obvious and not very good. Eh, but it's worked for
me for some time.

I'd recommend you just rip it out, stick it in your own repo.

## How

Just use the files. There's also a meson.build file, so if you want to
install it globally, do (from this directory):

```
meson build
ninja -Cbuild install
```

You'll  need Meson, of course, which you can get from your package
manager.
