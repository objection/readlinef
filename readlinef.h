#pragma once

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <readline/readline.h>
#include <readline/history.h>

char *readlinef (bool _add_history, char *fmt, ...);
char *readlinef_ap (bool _add_history, char *fmt, va_list ap);
bool readlinef_confirm (char *fmt, ...);
bool readlinef_confirm_ap (char *fmt, va_list ap);

