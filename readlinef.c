#define _GNU_SOURCE
#include "readlinef.h"
#include <assert.h>

char *readlinef_ap (bool _add_history, char *fmt, va_list ap) {
	char *msg = 0;

	// Not checking vasprintf return val.
	vasprintf (&msg, fmt, ap);
	char *r = readline (msg);
	if (!r)
		return 0;


	// Readline will return an empty string if you just hit return; it
	// will return all spaces if you enter those. Let's just say those
	// are not valid input, and free the string and return 0 if those
	// cases are the case. This means you the library user (me) just
	// needs to check for 0. I think this is reasonable.
	bool got_non_space = 0;
	for (char *_ = r; *_; _++) {
		if (*_ != '\t' && *_ != ' ' && *_ != '\n') {
			got_non_space = 1;
			break;
		}
	}
	if (!got_non_space) {
		free (r);
		return 0;
	}

	if (_add_history)
		add_history (r);
	free (msg);
	return r;
}

char *readlinef (bool _add_history, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	char *r = readlinef_ap (_add_history, fmt, ap);
	va_end (ap);
	return r;
}

bool readlinef_confirm_ap (char *fmt, va_list ap) {
	char *msg = NULL;
	// Not checking vasprintf return val.
	vasprintf (&msg, fmt, ap);
	char *p;
	enum {WAS_NO = 1, WAS_YES} was = 0;
	for (;;) {
		char *out = readline (msg);

		// If you ctrl-d, readline returns 0. So that's a "no", then.
		// The Readline manpage says nothing about other conditions
		// that can cause 0 to be returned.
		if (!out)
			return 0;
		p = out;
		while (isspace (*p)) p++;
		if (*p == 'y') {
			p++;
			while (isspace (*p)) p++;
			if (*p == '\0') {
				was = WAS_YES;
				free (out);
				break;
			}
		} else if (*p == 'n') {
			p++;
			while (isspace (*p)) p++;
			if (*p == '\0') {
				was = WAS_NO;
				free (out);
				break;
			}
		}
		free (out);
	}
	assert (was);
	free (msg);
	if (was == WAS_YES) return 1;
	else return 0;
}

// Expects a proper 'y' or 'n' or '   y ' or 'n ', etc.
bool readlinef_confirm (char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	bool r = readlinef_confirm_ap (fmt, ap);
	va_end (ap);
	return r;
}
